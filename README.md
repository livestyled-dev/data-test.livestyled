# Data Test

For this test you are to design a database schema and write queries against it.
You are free to use any database engine you wish.
*All* scripts should be placed within the `scripts` directory.
Please send your zipped solution via email.

## Database Restructure

Within the `data` folder are a few csv files, which you are to assume make up the current database.
You are to provide a full database schematic based upon the given scenario.
The scenario is as follows:

#### Scenario

I am the CFO of LiveStyled. I know our system allows customers to purchase 'offers', thus creating orders. 
We currently keep track of which event the customer was at when creating orders.
These orders can either be paid for via card payment or by using some of the user's credit, which we keep track of in our system. Credit is a monetary amount that is saved in their app user account and can be used to purchase items within the app.
A customer will only ever be credited if:

1. They purchased an offer, which was for credit (e.g. a £10 credit voucher)
2. They were credited for spending over £X (they should be credited 10% of X for orders over £30)
3. They were refunded

Being in charge of the company's finances, the CFO would like to be able to view a full history of orders and also know the state of an order at any given time.
He would also like to be able to view a user's credit history, and would like to know how much credit a customer has at any given time.
Some of our customers are from Sweden and Germany, so he would also like to store multiple languages for every offer, as well as different currencies. 

#### The Task

First you need to create a new database schema, then we'll need to parse the data from the CSV's into a new database.
The data within the CSV is not fully normalized, so you will need to make assumptions along the way.
Using *any language*, write a script to parse the CSV into the database.

The CFO is not a data engineer and can't write SQL, so he would like you to provide him with some reports.
Seeing as I am going to be running these queries every day, it would be better if you wrote me a script, which takes parameters to help me filter the data.
It should be worth noting that this database will grow considerably, so optimal performance is needed for these queries.
The script should be able to accomplish the following:

1. The top 5 customers who have spent the most, including the data of what they have bought, and also the total spend for all customers, within a given timeframe.
2. The top 2 highest spending customers for each event, including a the customers total spend for each event, within a given timeframe.
3. The top 2 highest grossing currencies and their top 3 highest selling offers.