#!/usr/bin/env bash

apt-get -y update
apt-get -y upgrade
apt-get -y autoremove
apt-get install -y mysql-server mysql-client
